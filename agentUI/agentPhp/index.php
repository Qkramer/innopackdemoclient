<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>CafeX Prototype Demonstration Page</title>

    <!-- Bootstrap core CSS -->
    <link href="/_inc/css/bootstrap.min.css" rel="stylesheet">
    <link href="/_inc/css/style.css" rel="stylesheet">
    <link href="/_inc/css/jquery-ui.min.css" rel="stylesheet">
    <link href="/_inc/css/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/_inc/css/jquery-ui.theme.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/_inc/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/_inc/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/_inc/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="/_inc/css/carousel.css" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Client Interface Demo</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Interfaces <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="dropdown-header">Customer-Facing</li>
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="dropdown-header">Internal</li>
                    <li><a href="#">PHP-Based Agent Console</a></li>
                    <li><a href="#">Single-Page Application AC</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>


    <div class="container" style="margin-left: auto; margin-right: auto;">
    <ul class="nav nav-tabs" style="margin-top: 10rem;" id="agentTabs">
      <li role="presentation" class="active">
          <a href="/" aria-controls="home" role="tab" data-toggle="tab" class="embed_clear">Home</a>
      </li>
      <li role="presentation">
          <a href="#agent" aria-controls="agent" role="tab" data-toggle="tab" class="embed_clear">Agent Console</a>
      </li>
      <li role="presentation">
          <a href="#crm" aria-controls="crm" role="tab" data-toggle="tab" class="embed_clear">CRM Statistics</a></li>
      <li role="presentation">
          <a href="#laci" aria-controls="laci" role="tab" data-toggle="tab" id="embedded_toggle">LA Console (iFrame)</a>
      </li>
      <li role="presentation">
          <a href="#" id="modal_window" role="tab" class="embed_clear">LA Console (Modal)</a>
      </li>
      <li role="presentation">
          <a href="" onclick="window.open( 'https://dev.rtcdemo.com/laclient-ui/agent', 'windowName' ); return false;" target="_blank" class="embed_clear">LA Console (New Tab)</a>
      </li>
      <li role="presentation">                                  
          <a href="" onclick="window.open('https://dev.rtcdemo.com/laclient-ui/agent', 'newwindow', 'width=300, height=250'); return false;" target="_new" class="embed_clear">LA Console (New Window)</a>
      </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content" style="min-height: 70rem;">
        <div role="tabpanel" class="tab-pane active" id="home">
            <h1 style="margin-left: 3rem;">Home Content Goes Here</h1>
        </div>
        <div role="tabpanel" class="tab-pane" id="agent">
            I'm an agent client interface!
        </div>
        <div role="tabpanel" class="tab-pane" id="crm">
            I'm a pretend CRM window.
        </div>
        <div role="tabpanel" class="tab-pane" id="laci">
            <div class="embed-responsive embed-responsive-16by9" id="embedded_frame">
                  
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="lacm">
            I don't exist because a modal dialog opened above me!
        </div>
    </div>
      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017 CafeX Communications, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div><!-- /.container -->

    <div id="agent_modal" title="Modal Agent Window" style="display: none;">
        
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/_inc/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="/_inc/js/vendor/jquery-ui.min.js"></script>
    <script src="/_inc/js/bootstrap.min.js"></script>
    <script src="/_inc/js/agentConsole.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="/_inc/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/_inc/js/ie10-viewport-bug-workaround.js"></script>
    <script type="text/javascript">
        $( document ).on( 'ready', function() {
            $('#agentTabs a').click( function (e) {
                e.preventDefault();
                $( this ).tab( 'show' );
            });        
        });
    </script>
  </body>
</html>
