$( document ).on( 'ready', function(){
    $( '#embedded_toggle' ).on( 'click', function(){
        $( 'div#embedded_frame' ).html( '<iframe class="embed-responsive-item" src="https://dev.rtcdemo.com/laclient-ui/agent"></iframe>' );
        return true;
    });

    $( '.embed_clear' ).on( 'click', function(){
        $( 'div#embedded_frame' ).html( '' );
        return true;
    });
    
    
    $( '#modal_window' ).on( 'click', function(event){
        event.preventDefault();
        $( '#agent_modal' ).html('<iframe class="embed-responsive-item" src="https://dev.rtcdemo.com/laclient-ui/agent" style="width:1345px; height: 775px; border: none;"></iframe>');
        $( '#agent_modal' ).dialog( 'open' );
        return true;    
    });

    $( '#agent_modal' ).dialog({
        autoOpen: false,
        height: 900,
        width: 1400,
        modal: true,
        buttons: [
          {
            text: "Done",
            icons: {
              primary: "ui-icon-heart"
            },
            click: function() {
              $( this ).dialog( "close" );
            }
       
            // Uncommenting the following line would hide the text,
            // resulting in the label being used as a tooltip
            //showText: false
          }
        ]
    });
});