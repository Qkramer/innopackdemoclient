import "angular2-meteor-polyfills";

import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { enableProdMode }         from "@angular/core";
import { Meteor }                 from "meteor/meteor";
import { AppModule }              from "./imports/app";

// The browser platform without a compiler
//import { platformBrowser } from '@angular/platform-browser';

// The app module factory produced by the static offline compiler
//import { AppModuleNgFactory } from './imports/app/app.module.ngfactory';

import '/both/methods/calls.methods';
import '/both/methods/capabilities.methods';
import '/both/methods/CxSession.methods';
import '/both/methods/emails.methods';
import '/both/methods/integrations.methods';
import '/both/methods/libraries.methods';
import '/both/methods/libraryItems.methods';
import '/both/methods/mailboxes.methods';
import '/both/methods/noules.methods';
import '/both/methods/noupes.methods';
import '/both/methods/pbxes.methods';
import '/both/methods/platforms.methods';
import '/both/methods/providers.methods';
import '/both/methods/records.methods';
import '/both/methods/skypes.methods';
import '/both/methods/teams.methods';
import '/both/methods/telements.methods';
import '/both/methods/users.methods';
import '/both/methods/voicemails.methods';
import '/both/methods/userSessions.methods';
import '/both/methods/commsObjects/enterpriseMessages.methods';
import '/both/methods/commsObjects/videoMailMessages.methods';

//enableProdMode();

Meteor.startup(() => {
    platformBrowserDynamic().bootstrapModule(AppModule);
    // Launch with the app module factory.
    //platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
});