<?php
/** Absolute path to the Current directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

require_once( './classes/application.php' );

$inputFile =  ABSPATH . "/input.json";
$outputFile = ABSPATH . "/test.json";

global $AngularApp;
$AngularApp = new GeneratedApplication( $inputFile );

include_once( './landing.html' );
  echo 'AbsPath is: ' . ABSPATH;
  echo '<br />All done';
?>
