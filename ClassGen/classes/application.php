<?php
/**
 * Name: application.php
 * URI:  http://client.rtcdemo.com
 * Description: Class for Generated application - both in construction and runtime
 * Author:      Quentin Kramer
 */
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $AngularApp;

  // Defining new application
  class GeneratedApplication {
      var $App_Array = array();
      var $Output_Array = array();
      var $AppName;
      
      public function __construct( $inputFile ) {

          $headers = array( 'http'=>array('method'=>'GET','header'=>'Content: type=application/json \r\n'.'$agent \r\n'.'$hash' ) );          
          $context=stream_context_create( $headers );
          
          $jsonArray = json_decode( file_get_contents( $inputFile ), true );
          return;
      }      
    
      private function load_many() {
          echo 'All done loading!';    
      }
      
      private function write_output() {
          file_put_contents( $outputFile, json_encode( $jsonArray, JSON_PRETTY_PRINT ) );    
      }
  }
?>